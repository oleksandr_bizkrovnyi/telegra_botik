package com.bizkrovnyi.island.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "bot")
@Component
@Data
public class Properties {
    private String token;
    private String username;
}
