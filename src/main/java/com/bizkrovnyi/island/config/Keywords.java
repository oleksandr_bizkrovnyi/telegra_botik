package com.bizkrovnyi.island.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@ConfigurationProperties(prefix = "keyword")
@Component
@Data
public class Keywords {
  private String endMessage;
  private String startMessage;
  private String finishKeyword;
  private String continueMessage;
  private List<String> allKeys;
}
