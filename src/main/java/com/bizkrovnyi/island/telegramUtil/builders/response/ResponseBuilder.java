package com.bizkrovnyi.island.telegramUtil.builders.response;

import com.bizkrovnyi.island.domain.AbstractStory;
import com.bizkrovnyi.island.domain.Answer;
import com.bizkrovnyi.island.domain.Question;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

public class ResponseBuilder {

    private SendMessage response = new SendMessage();

    public ResponseBuilder to(Long chatId) {
        response.setChatId(chatId);
        return this;
    }

    public ResponseBuilder setText(String text) {
        response.setText(text);
        return this;
    }

    public ResponseBuilder setKeyboard(ReplyKeyboardMarkup keyboard) {
        response.setReplyMarkup(keyboard);
        return this;
    }


    public SendMessage build() {
        return response;
    }
}
