package com.bizkrovnyi.island.telegramUtil.builders.keyboard;

import com.bizkrovnyi.island.config.Keywords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

@Service
public class KeyboardBuilder {


    private final Keywords keywords;
    private ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup();

    @Autowired
    public KeyboardBuilder( Keywords keywords) {
        this.keywords = keywords;
    }


    public KeyboardBuilder setButtons(List<String> buttonText) {
        List<KeyboardRow> rows = new ArrayList<>();
        buttonText.forEach(item -> {
            KeyboardRow row = new KeyboardRow();
            row.add(new KeyboardButton().setText(item));
            rows.add(row);
        });
        keyboard.setKeyboard(rows);
        return this;
    }

    public KeyboardBuilder empty() {
        List<KeyboardRow> rows = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        row.add(new KeyboardButton().setText(keywords.getFinishKeyword()));
        rows.add(row);
        keyboard.setKeyboard(rows);
        return this;
    }

    public ReplyKeyboardMarkup build() {
        return keyboard;
    }
}
