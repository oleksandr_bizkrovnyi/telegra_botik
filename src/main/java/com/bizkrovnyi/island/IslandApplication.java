package com.bizkrovnyi.island;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.bizkrovnyi.island")
@EnableJpaRepositories(basePackages="com.bizkrovnyi.island")
@EntityScan(basePackages = "com.bizkrovnyi.island.domain")
public class IslandApplication {

	public static void main(String[] args) {
		SpringApplication.run(IslandApplication.class, args);
	}

}
