package com.bizkrovnyi.island.repository;

import com.bizkrovnyi.island.domain.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer,String> {

    Answer findByDescriptionContaining(String description);
}
