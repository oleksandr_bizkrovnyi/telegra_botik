package com.bizkrovnyi.island.repository;

import com.bizkrovnyi.island.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, String> {

    Question findByDescriptionContaining(@Param("description") String description);
}
