package com.bizkrovnyi.island.domain;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "ANSWER")
@Data
public class Answer  extends AbstractStory{
    @Id
    @Column(name = "ANSWER_ID")
    private String id;

    @Column(unique = true, name = "DESCRIPTION", length = 99999999)
    private String description;

    @ManyToMany(cascade = {
            CascadeType.ALL
    },fetch = FetchType.EAGER, targetEntity = Question.class)
    @JoinTable(name = "QUESTION_ANSWER",
            joinColumns = @JoinColumn(name = "ANSWER_ID"),
            inverseJoinColumns = @JoinColumn(name = "QUESTION_ID")
    )
    private Set<? extends AbstractStory> stories;


    @Override
    public String toString() {
        return "Answer{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return id.equals(answer.id) &&
                description.equals(answer.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description);
    }
}
