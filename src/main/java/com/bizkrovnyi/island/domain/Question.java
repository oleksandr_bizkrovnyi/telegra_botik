package com.bizkrovnyi.island.domain;


import lombok.Data;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "QUESTION")
@Data
public class Question extends AbstractStory {

    @Id
    @Column(name = "QUESTION_ID")
    private String id;

    @Column(name = "DESCRIPTION", unique = true, length = 999999999)
    private String description;


    @ManyToMany(mappedBy = "stories",targetEntity = Answer.class)
    private Set<? extends AbstractStory> stories;

    @Override
    public String toString() {
        return "Question{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return id.equals(question.id) &&
                description.equals(question.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description);
    }
}