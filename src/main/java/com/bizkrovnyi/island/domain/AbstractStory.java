package com.bizkrovnyi.island.domain;

import lombok.Data;

import java.util.Set;
@Data
public class AbstractStory {
    private String id;
    private String description;
    private Set<? extends AbstractStory> stories;
}
