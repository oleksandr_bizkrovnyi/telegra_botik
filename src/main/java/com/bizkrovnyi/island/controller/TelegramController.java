package com.bizkrovnyi.island.controller;

import com.bizkrovnyi.island.config.Properties;
import com.bizkrovnyi.island.service.impl.TelegramServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;


@Component
@Slf4j
public class TelegramController extends TelegramLongPollingBot {

    private final Properties properties;
    private final TelegramServiceImpl telegramServiceImpl;

    @Autowired
    public TelegramController(Properties properties, TelegramServiceImpl telegramServiceImpl) {
        this.properties = properties;
        this.telegramServiceImpl = telegramServiceImpl;
    }

    @Override
    public String getBotToken() {
        return properties.getToken();
    }

    @Override
    public String getBotUsername() {
        return properties.getUsername();
    }

    @Override
    public void onUpdateReceived(Update update) {
        SendMessage sendMessage = telegramServiceImpl.prepareMessage(update);
        if(sendMessage != null){
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                log.error("Failed to send message  due to error: {}",  e.getMessage());
            }
        }
    }

    @PostConstruct
    public void start() {
        log.info("username: {}, token: {}", getBotUsername(), getBotToken());
    }

}
