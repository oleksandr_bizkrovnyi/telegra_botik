package com.bizkrovnyi.island.service.impl;

import com.bizkrovnyi.island.config.Keywords;
import com.bizkrovnyi.island.domain.AbstractStory;
import com.bizkrovnyi.island.domain.Answer;
import com.bizkrovnyi.island.domain.Question;
import com.bizkrovnyi.island.exceptions.UpdateNotFoundException;
import com.bizkrovnyi.island.repository.AnswerRepository;
import com.bizkrovnyi.island.repository.QuestionRepository;
import com.bizkrovnyi.island.service.TelegramService;
import com.bizkrovnyi.island.telegramUtil.builders.keyboard.KeyboardBuilder;
import com.bizkrovnyi.island.telegramUtil.builders.response.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class TelegramServiceImpl implements TelegramService {

    private final QuestionRepository repository;
    private final Keywords keywords;
    private final KeyboardBuilder keyboardBuilder;
    private final AnswerRepository answerRepository;


    @Autowired
    public TelegramServiceImpl(QuestionRepository repository, Keywords keywords, KeyboardBuilder keyboardBuilder, AnswerRepository answerRepository) {
        this.repository = repository;
        this.keywords = keywords;
        this.keyboardBuilder = keyboardBuilder;
        this.answerRepository = answerRepository;
    }

    public SendMessage prepareMessage(Update update) {
        String messageText = getMessageText(update);
        if (update.hasMessage()) {
            if (messageText.contains(keywords.getStartMessage())) {
                Question start = repository.findById("1").orElse(null);
                return createStartMessage(start, update.getMessage().getChatId());
            } else {
                Question question = repository.findByDescriptionContaining(messageText);
                Answer answer = answerRepository.findByDescriptionContaining(messageText);
                if (answer == null) {
                    return createStory(question, update.getMessage().getChatId());
                } else {
                    return createStory(answer, update.getMessage().getChatId());
                }
            }
        } else if (messageText.contains(keywords.getEndMessage())) {
            return createFinishMessage(update.getMessage().getChatId());
        }
        throw new UpdateNotFoundException("Update not found");
    }

    private SendMessage createStory(AbstractStory story, Long chatId) {
        if (story == null) {
            throw new NullPointerException("question doesn`t exist");
        }
        List<String> questions = createKeyboardString(story);
        if (questions.size() == 0) {
            return createFinishMessage(story, chatId);
        }
        if (questions.size() > 1) {
            ReplyKeyboardMarkup keyboard = keyboardBuilder.setButtons(questions).build();
            return new ResponseBuilder()
                    .to(chatId)
                    .setKeyboard(keyboard)
                    .setText(createResponseTextFromStories(questions))
                    .build();
        } else {
            Question question = repository.findByDescriptionContaining(questions.get(0));
            Answer answer = answerRepository.findByDescriptionContaining(questions.get(0));
            if (question == null) {
                questions = createKeyboardString(answer);
                return createNextMessage(answer, chatId, questions);
            } else {
                questions = createKeyboardString(question);
                return createNextMessage(question, chatId, questions);
            }
        }
    }

    private SendMessage createNextMessage(AbstractStory story, Long chatId, List<String> questions) {
        questions = createKeyboardString(story);
        if (questions.size() != 0) {
            if (questions.get(0).length() > 127) {
                ReplyKeyboardMarkup keyboard = keyboardBuilder.setButtons(questions).build();
                return new ResponseBuilder()
                        .to(chatId)
                        .setKeyboard(keyboard)
                        .setText(story.getDescription() + System.lineSeparator() + questions.get(0))
                        .build();
            } else {
                ReplyKeyboardMarkup keyboard = keyboardBuilder.setButtons(questions).build();
                return new ResponseBuilder()
                        .to(chatId)
                        .setKeyboard(keyboard)
                        .setText(story.getDescription())
                        .build();
            }

        }
        ReplyKeyboardMarkup keyboard = keyboardBuilder.setButtons(Stream.of(keywords.getEndMessage()).collect(Collectors.toList())).build();
        return new ResponseBuilder()
                .to(chatId)
                .setKeyboard(keyboard)
                .setText(story.getDescription())
                .build();
    }


    private String createResponseTextFromStories(List<String> questions) {
        StringBuilder result = new StringBuilder();
        if (questions.size() > 1) {
            result.append("[____ ВЫБИРАЙ____]").append(System.lineSeparator());
        } else {
            Question question = repository.findByDescriptionContaining(questions.get(0));
            Answer answer = answerRepository.findByDescriptionContaining(questions.get(0));
            if (answer == null) {
                return question.getDescription();
            } else {
                return answer.getDescription();
            }
        }
        return result.toString();
    }

    private List<String> createKeyboardString(AbstractStory abstractStory) {
        return abstractStory.getStories()
                .stream().filter(question -> filterStory(question, abstractStory))
                .map(AbstractStory::getDescription)
                .collect(Collectors.toList());

    }

    private SendMessage createFinishMessage(AbstractStory answer, Long chatId) {
        return new ResponseBuilder().to(chatId)
                .setKeyboard(keyboardBuilder.empty().build())
                .setText(answer.getDescription())
                .build();
    }

    private SendMessage createFinishMessage(Long chatId) {
        return new ResponseBuilder().to(chatId)
                .setKeyboard(keyboardBuilder.empty().build())
                .setText("to be continue")
                .build();
    }

    private SendMessage createStartMessage(Question question, Long chatId) {
        if (question == null) {
            throw new NullPointerException("Question isn`t exist");
        }
        List<String> answers = question.getStories()
                .stream()
                .map(AbstractStory::getDescription)
                .collect(Collectors.toList());
        ReplyKeyboardMarkup keyboard = keyboardBuilder
                .setButtons(answers)
                .build();
        return new ResponseBuilder()
                .to(chatId)
                .setKeyboard(keyboard)
                .setText(question.getDescription())
                .build();
    }

    private boolean filterStory(AbstractStory story, AbstractStory answer) {
        Matcher questionMatcher = Pattern.compile("\\d{0,2}").matcher(story.getId());
        Matcher answerMatcher = Pattern.compile("\\d{0,2}").matcher(answer.getId());
        if (questionMatcher.find() && answerMatcher.find()) {
            int questionMajorId = Integer.parseInt(questionMatcher.group());
            int answerMajorId = Integer.parseInt(answerMatcher.group());
            if (questionMajorId == answerMajorId) {
                return story.getId().length() > answer.getId().length();
            }
            return questionMajorId > answerMajorId;
        }
        throw new IllegalArgumentException("Id is not a number");
    }

    private String getMessageText(Update update) {
        Matcher matcher = Pattern.compile(".+\\…$").matcher(update.getMessage().getText());
        if (matcher.find()) {
            return update.getMessage().getText().substring(0, update.getMessage().getText().length() - 1);
        }
        return update.getMessage().getText();
    }


}
