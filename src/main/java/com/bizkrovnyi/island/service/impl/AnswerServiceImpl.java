package com.bizkrovnyi.island.service.impl;

import com.bizkrovnyi.island.domain.Answer;
import com.bizkrovnyi.island.repository.AnswerRepository;
import com.bizkrovnyi.island.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;

    @Autowired
    public AnswerServiceImpl(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @Override
    public Answer getAnswerByDesc(String desc) {
        return answerRepository.findByDescriptionContaining(desc);
    }
}
