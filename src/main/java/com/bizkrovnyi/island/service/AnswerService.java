package com.bizkrovnyi.island.service;

import com.bizkrovnyi.island.domain.Answer;

public interface AnswerService {
    Answer getAnswerByDesc(String desc);
}
