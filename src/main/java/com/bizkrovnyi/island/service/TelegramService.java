package com.bizkrovnyi.island.service;

import com.bizkrovnyi.island.domain.Question;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface TelegramService {
    SendMessage prepareMessage(Update update) ;

}
